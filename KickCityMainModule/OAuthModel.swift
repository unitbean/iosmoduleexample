//
//  OAuthModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 08/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class OAuthModel: NSObject {
    
    var tokenType:String?
    var accessToken:String?
    var expireDate:NSDate?
    var refreshToken:String?
    
     var error:String?

}
