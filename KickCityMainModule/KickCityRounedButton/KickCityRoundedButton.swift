//
//  KickCityRoundedButton.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class KickCityRoundedButton: UIButton {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override func drawRect(rect: CGRect) {
        // Drawing code

    }
    

    
    @IBInspectable internal var borderColor: UIColor = UIColor.clearColor() {
        didSet {
            layer.borderColor = borderColor.CGColor
        }
    }
    
    @IBInspectable internal var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable internal var cornerRadius: CGFloat = 2 {
        didSet {
            if self.cornerRadius == -1 {
                layer.cornerRadius = self.frame.height / 2
              
            }else{
              //  layer.cornerRadius = self.frame.height / 2 =
                layer.cornerRadius = self.cornerRadius
            }
            layer.masksToBounds = true

        }
    }

}
