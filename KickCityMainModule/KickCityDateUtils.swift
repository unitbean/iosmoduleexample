//
//  KickCityDayeUtils.swift
//  KickCity
//
//  Created by Nabiullin Anton on 09/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class KickCityDateUtils: NSObject {
    
    static func getCurrentDateByAddingSeconds(seconds:Int) -> NSDate {
    
        let calendar = NSCalendar.currentCalendar()
        let date = calendar.dateByAddingUnit(.Minute, value: seconds, toDate: NSDate(), options: [])
        return date!
    }

}
