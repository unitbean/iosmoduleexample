//
//  KickCityAlertUtils.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class KickCityAlertUtils: NSObject {
    
    static func showSimpleAlertWithTitle<VC:UIViewController>(title:String, fromVC:VC) {
        
        let alertController = UIAlertController(title: "Error", message:
            title, preferredStyle: UIAlertControllerStyle.Alert)
        alertController.addAction(UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.Default,handler: nil))
        
        fromVC.presentViewController(alertController, animated: true, completion: nil)
    }

}
