//
//  NSDateExtesion.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//
import DateTools


extension NSDate {
    func isGreaterThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isGreater = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedDescending {
            isGreater = true
        }
        
        //Return Result
        return isGreater
    }
    
    func isLessThanDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isLess = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedAscending {
            isLess = true
        }
        
        //Return Result
        return isLess
    }
    
    func equalToDate(dateToCompare: NSDate) -> Bool {
        //Declare Variables
        var isEqualTo = false
        
        //Compare Values
        if self.compare(dateToCompare) == NSComparisonResult.OrderedSame {
            isEqualTo = true
        }
        
        //Return Result
        return isEqualTo
    }
    
    func addDays(daysToAdd: Int) -> NSDate {
        let secondsInDays: NSTimeInterval = Double(daysToAdd) * 60 * 60 * 24
        let dateWithDaysAdded: NSDate = self.dateByAddingTimeInterval(secondsInDays)
        
        //Return Result
        return dateWithDaysAdded
    }
    
    func addHours(hoursToAdd: Int) -> NSDate {
        let secondsInHours: NSTimeInterval = Double(hoursToAdd) * 60 * 60
        let dateWithHoursAdded: NSDate = self.dateByAddingTimeInterval(secondsInHours)
        
        //Return Result
        return dateWithHoursAdded
    }
    
    func getFeedDateFormat()->String {
        let formatter = NSDateFormatter()
        if self.isToday() {
            formatter.dateFormat = "Today, hh:mm"
        }else if self.isTomorrow() {
            formatter.dateFormat = "Tomorrow, hh:mm"
        }else{
            formatter.dateFormat = "dd MMMM, hh:mm"
        }
 
        
        
        return formatter.stringFromDate(self)
    }
    
    static func getDateFromFormmatedString(stringDate:String) -> NSDate {
        
        
        let dateFormatter:NSDateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"

        return dateFormatter.dateFromString(stringDate)!
  
    }
    
    
    //TIME AGO
    // shows 1 or two letter abbreviation for units.
    // does not include 'ago' text ... just {value}{unit-abbreviation}
    // does not include interim summary options such as 'Just now'
    public var timeAgoSimple: String {
        let components = self.dateComponents()
        
        if components.year > 0 {
            return stringFromFormat("%%d%@yr", withValue: components.year)
        }
        
        if components.month > 0 {
            return stringFromFormat("%%d%@mo", withValue: components.month)
        }
        
        // TODO: localize for other calanders
        if components.day >= 7 {
            let value = components.day/7
            return stringFromFormat("%%d%@w", withValue: value)
        }
        
        if components.day > 0 {
            return stringFromFormat("%%d%@d", withValue: components.day)
        }
        
        if components.hour > 0 {
            return stringFromFormat("%%d%@h", withValue: components.hour)
        }
        
        if components.minute > 0 {
            return stringFromFormat("%%d%@m", withValue: components.minute)
        }
        
        if components.second > 0 {
            return stringFromFormat("%%d%@s", withValue: components.second )
        }
        
        return ""
    }
    
    public var timeAgo: String {
        let components = self.dateComponents()
        
        if components.year > 0 {
            if components.year < 2 {
                return "Last year"
            } else {
                return stringFromFormat("%%d%@y", withValue: components.year)
            }
        }
        
        if components.month > 0 {
            if components.month < 2 {
                return "Last month"
            } else {
                return stringFromFormat("%%d%@m", withValue: components.month)
            }
        }
        
        // TODO: localize for other calanders
        if components.day >= 7 {
            let week = components.day/7
            if week < 2 {
                return "Last week"
            } else {
                return stringFromFormat("%%d%@w", withValue: week)
            }
        }
        
        if components.day > 0 {
            if components.day < 2 {
                return "1d"
            } else  {
                return stringFromFormat("%%d%@d", withValue: components.day)
            }
        }
        
        if components.hour > 0 {
            if components.hour < 2 {
                return "1h"
            } else  {
                return stringFromFormat("%%d%@h", withValue: components.hour)
            }
        }
        
        if components.minute > 0 {
            if components.minute < 2 {
                return "1m"
            } else {
                return stringFromFormat("%%d%@m", withValue: components.minute)
            }
        }
        
        if components.second > 0 {
            if components.second < 5 {
                return "now"
            } else {
                return stringFromFormat("%%d%@s", withValue: components.second)
            }
        }
        
        return ""
    }
    
    private func dateComponents() -> NSDateComponents {
        let calander = NSCalendar.currentCalendar()
        return calander.components([.Second, .Minute, .Hour, .Day, .Month, .Year], fromDate: self, toDate: NSDate(), options: [])
    }
    
    private func stringFromFormat(format: String, withValue value: Int) -> String {
        let localeFormat = String(format: format, getLocaleFormatUnderscoresWithValue(Double(value)))
        return localeFormat
    }
    
    private func getLocaleFormatUnderscoresWithValue(value: Double) -> String {
        guard let localeCode = NSLocale.preferredLanguages().first else {
            return ""
        }
        
        // Russian (ru) and Ukrainian (uk)
        if localeCode == "ru" || localeCode == "uk" {
            let XY = Int(floor(value)) % 100
            let Y = Int(floor(value)) % 10
            
            if Y == 0 || Y > 4 || (XY > 10 && XY < 15) {
                return ""
            }
            
            if Y > 1 && Y < 5 && (XY < 10 || XY > 20) {
                return "_"
            }
            
            if Y == 1 && XY != 11 {
                return "__"
            }
        }
        
        return ""
    }
    
    func converDateToStringApiFormat() -> String {

        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
        let dateString = dateFormatter.stringFromDate(self)
        print(dateString) //prints out 10:12
        return dateString
    }
    
    func getMonthName()->String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "MMMM"
        return formatter.stringFromDate(self)
        
    }
    func getDayStringNumber()->String {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "dd"
        return formatter.stringFromDate(self)
        
    }
    
}