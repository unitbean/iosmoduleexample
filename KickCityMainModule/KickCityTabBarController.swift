//
//  KickCityTabBarController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 16/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class KickCityTabBarController: UITabBarController, UITabBarControllerDelegate {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tabBarController(tabBarController: UITabBarController, shouldSelectViewController viewController: UIViewController) -> Bool {
        if viewController.isKindOfClass(CreateEventNavigationController) {
            
            
            let vc = UIStoryboard(name: "CreateEvent", bundle: nil).instantiateInitialViewController()
            self.presentViewController(vc!, animated: true, completion: nil)
            
            return false
        }
        return true
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
