//
//  KickCityApiService.swift
//  KickCity
//
//  Created by Nabiullin Anton on 08/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

public typealias GetNewTockenByRefreshTokenComplection = ( newToken:String? ) -> Void
public typealias RequestComplection = (results:JSON?) -> Void

class KickCityApiService: NSObject {
    
    internal let host:String = "http://kc.looi.ru/api"
    
    /**
     Returns abs url for request
     
     @param Url
     
     @return abs url
     */
    internal func url(url:String) -> String {
        return host + url
    }
    
    
    /**
     Returns true if token good
     
     eturn true if token good
     */
    internal func checkToken() -> Bool {
        
        let expiredDateToken = UserDefaultsUtils.getRefreshTokenDate()
        
        if NSDate().isGreaterThanDate(expiredDateToken!) {
            return false
        }else{
            //TODO: проверить токены пр добавдлении даты
            return false
        }
        
        
    }
    
    internal func saveTokenData(data:OAuthModel) {
        UserDefaultsUtils.setCurrentToken(data.accessToken!)
        UserDefaultsUtils.setRefreshToken(data.refreshToken!)
        UserDefaultsUtils.setRefreshTokenDate(data.expireDate!)
    }
    
    
    
    func getNewTockenByRefreshToken(refreshToken:String, complection:GetNewTockenByRefreshTokenComplection ) {
        let url = self.url("/oauth/token")
        
        Alamofire.request(.POST, url,
            parameters: [
                "grant_type" : "refresh_token",
                "client_id": "777",
                "client_secret": "kickcitysecret",
                "refresh_token": refreshToken
            ],
            
            encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
                
                let result:OAuthModel = OAuthModel()
                switch response.result {
                    
                case .Success(let data):
                    //TODO: Изменить далее и проверить надо ли херачить остальные поля
                    let json:JSON = JSON(data)
                    
                     print(json)
                    if json["error"] != nil  {
                        
                        result.error = "Get new token problem"
                        complection(newToken: nil)
                        return
                        
                    }

                    
                    result.tokenType = json["token_type"].stringValue
                    result.accessToken = json["access_token"].stringValue
                    result.expireDate =  KickCityDateUtils.getCurrentDateByAddingSeconds((json["expires_in"].intValue))
                    result.refreshToken = json["refresh_token"].stringValue
                    
                    UserDefaultsUtils.setCurrentToken(result.tokenType!)
                    UserDefaultsUtils.setRefreshToken(result.refreshToken!)
                    UserDefaultsUtils.setRefreshTokenDate(KickCityDateUtils.getCurrentDateByAddingSeconds((json["expires_in"].intValue)))
                    
                    complection(newToken: result.accessToken)
                    
                    break
                    
                case .Failure(_):

                    result.error = "Error while getting from server"
                    complection(newToken: nil)
                    break
                    
                }
                
                
        }
        
        
    }
    
    internal func sendPostRequestAccessTokenHeader(requestType: Alamofire.Method,var url:String, params:[String: AnyObject]?, complection:RequestComplection) {
        print(params)
        if self.checkToken() == false {
            self.getNewTockenByRefreshToken(UserDefaultsUtils.getRefreshToken()!, complection: { (newToken) -> Void in
                
                url = url + "?access_token=" + newToken!
                self.alamofireReqShortCutJSONWithHeaders(requestType, url: url, params: params!, complection: complection)
            })
        }else{
            
            url = url + "?access_token=" + UserDefaultsUtils.getCurrentToken()!
            self.alamofireReqShortCutJSONWithHeaders(requestType, url: url, params: params!, complection: complection)
            
            
        }
    }
    
    internal func fileUpload(fileType:String, fileUrl:NSData, complection:RequestComplection) {
    //internal func fileUpload(fileType:String, fileUrl:NSURL, complection:RequestComplection) {
        
        if self.checkToken() == false {
            self.getNewTockenByRefreshToken(UserDefaultsUtils.getRefreshToken()!, complection: { (newToken) -> Void in
                
                let url = self.url("/file/\(fileType)?access_token=" + newToken!)
                self.fileUploadProcess(fileType, url: url, fileUrl: fileUrl, complection: { (results) -> Void in
                    complection(results: results)
                })
            })
        }else{
            
            //url = url + "?access_token=" + UserDefaultsUtils.getCurrentToken()!
            let url = self.url("/file/\(fileType)?access_token=" + UserDefaultsUtils.getCurrentToken()!)
            self.fileUploadProcess(fileType, url: url, fileUrl: fileUrl, complection: { (results) -> Void in
                complection(results: results)
            })
            
        }
        
    }
    
    private func fileUploadProcess(fileType:String, url:String, fileUrl:NSData, complection:RequestComplection ) {
   // private func fileUploadProcess(fileType:String, url:String, fileUrl:NSURL, complection:RequestComplection ) {
        
        
        
        Alamofire.upload(.POST, url,
            multipartFormData: { multipartFormData in
               // multipartFormData.appendBodyPart(fileURL: fileUrl, name: "event")
                multipartFormData.appendBodyPart(data: fileUrl, name: "event", fileName: "event.jpeg", mimeType: "image/jpeg")
            }, encodingCompletion: { encodingResult in
                switch encodingResult {
                case .Success(let upload, _, _):
                    upload.responseJSON { response in
                        switch response.result {
                        case .Success(let data):
                            //TODO: Изменить далее и проверить надо ли херачить остальные поля
                            let json:JSON = JSON(data)
                            print(json)
                            
                            complection(results: json)
                            
                            break
                            
                        case .Failure(_):
                            //  self.allFailure()
                            //TODO: Добавить complections
                            complection(results: nil)
                            break
                            
                        }
                    }
                case .Failure(let encodingError):
                    print(encodingError)
                }
        })

        
        
    }
    
    
    
    internal func sendRequest(requestType: Alamofire.Method, url:String, var params:[String: AnyObject]?, complection:RequestComplection ) {
        
        
        if self.checkToken() == false {
            self.getNewTockenByRefreshToken(UserDefaultsUtils.getRefreshToken()!, complection: { (newToken) -> Void in
                
                params!["access_token"] = newToken
                
               // params!["access_token"] = "777"

           
                
                 self.alamofireReqShortCut(requestType, url: url, params: params, complection: complection)
            })
        }else{
            
            params!["access_token"] = UserDefaultsUtils.getCurrentToken()
            
              //  params!["access_token"] = "777"
                
   
            self.alamofireReqShortCut(requestType, url: url, params: params, complection: complection)
            
            
        }

    
        
       
        
    }
    
    private func alamofireReqShortCutJSONWithHeaders(requestType: Alamofire.Method, url:String, params:[String: AnyObject], complection:RequestComplection) {
        
        let headers = [
            "Content-Type": "application/json"
        ]
        
        Alamofire.request(requestType, url,
            
            parameters: params,
            
            encoding: .JSON, headers: headers).responseJSON{ response in
                
                let result:OAuthModel = OAuthModel()
                debugPrint(response)
                switch response.result {
                    
                case .Success(let data):
                    
                    let json:JSON = JSON(data)
                    print(json)
                    
                    complection(results: json)
                    
                    break
                    
                case .Failure(_):
                    //  self.allFailure()
                    //TODO: Добавить complections
                    result.error = "Error while getting from server"
                    complection(results: nil)
                    break
                    
                }
                
                
        }
        
    }
    
    private func alamofireReqShortCut(requestType: Alamofire.Method, url:String, params:[String: AnyObject]?, complection:RequestComplection) {
        
        Alamofire.request(requestType, url,
            
            parameters: params,
            
            encoding: ParameterEncoding.URL, headers: nil).responseJSON{ response in
                
                let result:OAuthModel = OAuthModel()
                debugPrint(response)
                switch response.result {
                    
                case .Success(let data):

                    let json:JSON = JSON(data)
                    print(json)
                    
                    complection(results: json)
                    
                    break
                    
                case .Failure(_):
                    //  self.allFailure()
                    //TODO: Добавить complections
                    result.error = "Error while getting from server"
                    complection(results: nil)
                    break
                    
                }
                
                
        }

    }
    

}
