//
//  NotificationNavigationController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 01/03/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class NotificationNavigationController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        let vc = UIStoryboard(name: "NotificationViewController", bundle: nil).instantiateInitialViewController() as! NotificationViewController
        self.setViewControllers([vc], animated: false)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
