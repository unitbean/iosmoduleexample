//
//  KickCityEventWantToGoController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 01/03/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class KickCityEventWantToGoController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        let vc = UIStoryboard(name: "WantToGoStoryBoard", bundle: nil).instantiateInitialViewController() as! UserWantToGoViewController
        self.setViewControllers([vc], animated: false)
        
        self.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationBar.shadowImage = UIImage()
        self.navigationBar.translucent = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
