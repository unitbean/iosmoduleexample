//
//  KickCityServices.swift
//  KickCity
//
//  Created by Nabiullin Anton on 08/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class KickCityServices {
    

    private(set) var loginUserService:LoginUserService
    private(set) var feedService:FeedService
    private(set) var categoryService:SelectCategoryService
    private(set) var commentService:EventSingleServices
    private(set) var findLocationService:FindLocationService
    private(set) var createEventService:CreateEventService
    private(set) var profileViewService:ProfileViewService
    private(set) var userFollowinFollowersService:UserFollowinFollowersService
    
    init() {
        //Здесь инициализируем
        self.loginUserService = LoginUserService()
        self.feedService = FeedService()
        self.categoryService = SelectCategoryService()
        self.commentService = EventSingleServices()
        self.findLocationService = FindLocationService()
        self.createEventService = CreateEventService()
        self.profileViewService = ProfileViewService()
        self.userFollowinFollowersService = UserFollowinFollowersService()
    }
    
    
    /**
     Returns singleton instance of Services
     
     @param bar Consectetur adipisicing elit.
     
     @return Sed do eiusmod tempor.
     */
    class var sharedInstance: KickCityServices {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: KickCityServices? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = KickCityServices()
        }
        return Static.instance!
    }
    

    

}
