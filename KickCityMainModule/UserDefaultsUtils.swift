//
//  UserDefaultsUtils.swift
//  KickCity
//
//  Created by Nabiullin Anton on 09/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class UserDefaultsUtils: NSObject {
    
    
    
    static func setLogginedVkUser(vkId:String){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(vkId, forKey: "VK_ID")
        defaults.synchronize()
    }
    
    static func getLogginedVkUserId() -> String?{
        return NSUserDefaults.standardUserDefaults().stringForKey("VK_ID")
    }
    
    
    static func setLogginedFBUser(vkId:String){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(vkId, forKey: "FB_ID")
        defaults.synchronize()
    }
    
    static func getLogginedFBUserId() -> String?{
        return NSUserDefaults.standardUserDefaults().stringForKey("FB_ID")
    }
    
    
    static func setCurrentToken(currentToken:String){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(currentToken, forKey: "CURRENT_TOKEN")
        defaults.synchronize()
    }
    
    static func getCurrentToken() -> String?{
        return NSUserDefaults.standardUserDefaults().stringForKey("CURRENT_TOKEN")
    }
    
    static func setRefreshTokenDate(currentToken:NSDate){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(currentToken, forKey: "REFRESH_TOKEN_DATE")
        defaults.synchronize()
    }
    
    static func getRefreshTokenDate() -> NSDate?{
        return NSUserDefaults.standardUserDefaults().objectForKey("REFRESH_TOKEN_DATE") as? NSDate
    }
    
    
    static func setRefreshToken(currentToken:String){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(currentToken, forKey: "REFRESH_TOKEN")
        defaults.synchronize()
    }
    
    static func getRefreshToken() -> String?{
        return NSUserDefaults.standardUserDefaults().stringForKey("REFRESH_TOKEN")
    }
    
    
    static func setLongitude(long:Double){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(long, forKey: "LONGITUDE")
        defaults.synchronize()
    }
    static func setLattitude(latt:Double){
        let defaults = NSUserDefaults.standardUserDefaults()
        defaults.setObject(latt, forKey: "LATTITUDE")
        defaults.synchronize()
    }
    
    static func getLongitude() -> Double?{
         return 37.798546
        if NSUserDefaults.standardUserDefaults().doubleForKey("LONGITUDE") == 0 {
            return nil
        }
        return NSUserDefaults.standardUserDefaults().doubleForKey("LONGITUDE")
    }
    
    static func getLattitude() -> Double?{
        return 55.798546
        if NSUserDefaults.standardUserDefaults().doubleForKey("LATTITUDE") == 0 {
            return nil
        }
        return NSUserDefaults.standardUserDefaults().doubleForKey("LATTITUDE")
    }
    
    static func getCity() -> Double?{
        return 37.798546
        if NSUserDefaults.standardUserDefaults().doubleForKey("LONGITUDE") == 0 {
            return nil
        }
        return NSUserDefaults.standardUserDefaults().doubleForKey("LONGITUDE")
    }

}
