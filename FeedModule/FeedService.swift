//
//  FeedService.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias RequestAllEventsResult = (results: [FeedItemServiceModel]?, error:String?) -> Void

class FeedService:KickCityApiService {
    
    private let eventsRequestLimit = 10
    
    func requestUserGoingEvents(userId:String?, offset:Int, complection:RequestAllEventsResult) {
        
        let url = self.url("/db/events")
        
        var params:[String:AnyObject] = [String:AnyObject]()
        
        params["limit"] = eventsRequestLimit
        params["offset"] = offset
        
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseFeedObjectForProfile(results, complection: complection)
            
        }
    }
    
    
    func requestFindEvents(searchText:String, userId:String?, offset:Int, complection:RequestAllEventsResult) {
        let url = self.url("/search/event/feed/all")
        
        var params:[String:AnyObject] = [String:AnyObject]()
        
        params["limit"] = eventsRequestLimit
        params["offset"] = offset
        params["title"] = searchText
        
        if UserDefaultsUtils.getLattitude() != nil && UserDefaultsUtils.getLongitude() != nil {
            params["lat"] = UserDefaultsUtils.getLattitude()
            params["lon"] = UserDefaultsUtils.getLongitude()
        }
        
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseFeedObject(results, complection: complection)
            
        }
        
    }

    
    func requestAllEvents(userId:String?, offset:Int, complection:RequestAllEventsResult) {
        let url = self.url("/search/event/feed/all")
        
        var params:[String:AnyObject] = [String:AnyObject]()
       
        params["limit"] = eventsRequestLimit
        params["offset"] = offset
        
        if  UserDefaultsUtils.getLattitude() != nil && UserDefaultsUtils.getLongitude() != nil {
            params["lat"] = UserDefaultsUtils.getLattitude()
            params["lon"] = UserDefaultsUtils.getLongitude()
            
            print(params)
        }
        
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseFeedObject(results, complection: complection)
            
        }
   
    }
    
    func requestNowEvents(userId:String?, offset:Int, complection:RequestAllEventsResult) {
        let url = self.url("/search/event/feed/now")
        
        var params:[String:AnyObject] = [String:AnyObject]()
        
        params["limit"] = eventsRequestLimit
        params["offset"] = offset
        
        if UserDefaultsUtils.getLattitude() != nil && UserDefaultsUtils.getLongitude() != nil {
            params["lat"] = UserDefaultsUtils.getLattitude()
            params["lon"] = UserDefaultsUtils.getLongitude()
        }
        
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseFeedObject(results, complection: complection)
            
        }
    }
    
    func requestNearEvents(userId:String?, offset:Int, complection:RequestAllEventsResult) {
        let url = self.url("/search/event/feed/near")
        
        var params:[String:AnyObject] = [String:AnyObject]()
        
        params["limit"] = eventsRequestLimit
        params["offset"] = offset
        
        if UserDefaultsUtils.getLattitude() != nil && UserDefaultsUtils.getLongitude() != nil {
            params["lat"] = UserDefaultsUtils.getLattitude()
            params["lon"] = UserDefaultsUtils.getLongitude()
        }
        
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseFeedObject(results, complection: complection)
            
        }
    }
    
    func requestFriendsEvents(userId:String?, offset:Int, complection:RequestAllEventsResult) {
        let url = self.url("/search/event/feed/friends")
        
        var params:[String:AnyObject] = [String:AnyObject]()
        
        params["limit"] = eventsRequestLimit
        params["offset"] = offset
        
        if UserDefaultsUtils.getLattitude() != nil && UserDefaultsUtils.getLongitude() != nil {
            params["lat"] = UserDefaultsUtils.getLattitude()
            params["lon"] = UserDefaultsUtils.getLongitude()
        }
        
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseFeedObject(results, complection: complection)
            
        }
    }
    
    private func parseFeedObjectForProfile(data:JSON?, complection:RequestAllEventsResult) {
        
        
        if data == nil ||  data!["error"] != nil {
            complection(results:nil, error:"Error while getting data from api")
            return
        }
        print(data)
        var returnValue:[FeedItemServiceModel] = []
       // if data!["rows"].dictionaryValue["rows"] != nil {
            for resultItem in data!["rows"].arrayValue {
                print("////////HERE////\(resultItem)")
                let feedServiceItem:FeedItemServiceModel = FeedItemServiceModel()
                feedServiceItem.feedItemId = resultItem["id"].stringValue
                feedServiceItem.feedItemTitle = resultItem["title"].stringValue
                feedServiceItem.feedItemLattitude = resultItem["location"].dictionaryValue["lat"]?.doubleValue
                feedServiceItem.feedItemLongitude = resultItem["location"].dictionaryValue["lon"]?.doubleValue
                feedServiceItem.feedItemCity = resultItem["city"].stringValue
                feedServiceItem.feedItemPrivacy = FeedPrivacy(rawValue: resultItem["privacy"].stringValue)
                feedServiceItem.feedItemAddress = resultItem["address"].stringValue
                feedServiceItem.feedItemHashTag = resultItem["hashTag"].stringValue
                feedServiceItem.feedItemCategoryId = resultItem["categoryId"].stringValue
                feedServiceItem.feedItemCoverImage = resultItem["cover"].stringValue
                feedServiceItem.feedItemEventDetails = resultItem["eventDetails"].stringValue
                feedServiceItem.feedItemStartDateTime = NSDate.getDateFromFormmatedString(resultItem["startDateTime"].stringValue)
              //  feedServiceItem.feedItemEndDateTime = NSDate.getDateFromFormmatedString(resultItem["endDateTime"].stringValue)
                feedServiceItem.feedItemLink = resultItem["link"].stringValue
                feedServiceItem.feedItemEventLikes = resultItem["eventLike"].intValue
                feedServiceItem.feedItemCreatedAt = NSDate.getDateFromFormmatedString(resultItem["createdAt"].stringValue)
                feedServiceItem.feedItemUpdatedAt = NSDate.getDateFromFormmatedString(resultItem["updatedAt"].stringValue)
                
                feedServiceItem.feedItemFriendsCount = resultItem["going"].dictionaryValue["friendsCount"]?.intValue
                feedServiceItem.feedItemOtherCount = resultItem["going"].dictionaryValue["othersCount"]?.intValue
                
                feedServiceItem.feedItemCanEdit = resultItem["canEdit"].boolValue
                
                for avatar in (resultItem["going"].dictionaryValue["avatars"]?.arrayValue)! {
                    feedServiceItem.feedItemAvatars.append(avatar["avatar"].stringValue)
                }
                
                
                // '2015-04-13 12:30:00' (UTC +0)
                
                returnValue.append(feedServiceItem)
                
                
            }
       // }
        complection(results:returnValue, error:nil)
        
        
    }

    
    private func parseFeedObject(data:JSON?, complection:RequestAllEventsResult) {
        
        
        if data == nil ||  data!["error"] != nil {
            complection(results:nil, error:"Error while getting data from api")
            return
        }
        print(data)
        var returnValue:[FeedItemServiceModel] = []
        if data!["hits"].dictionaryValue["hits"] != nil {
            for resultItem in data!["hits"].dictionaryValue["hits"]!.arrayValue {
                print("////////HERE////\(resultItem)")
                let feedServiceItem:FeedItemServiceModel = FeedItemServiceModel()
                feedServiceItem.feedItemId = resultItem["id"].stringValue
                feedServiceItem.feedItemTitle = resultItem["title"].stringValue
                feedServiceItem.feedItemLattitude = resultItem["location"].dictionaryValue["lat"]?.doubleValue
                feedServiceItem.feedItemLongitude = resultItem["location"].dictionaryValue["lon"]?.doubleValue
                feedServiceItem.feedItemCity = resultItem["city"].stringValue
                feedServiceItem.feedItemPrivacy = FeedPrivacy(rawValue: resultItem["privacy"].stringValue)
                feedServiceItem.feedItemAddress = resultItem["address"].stringValue
                feedServiceItem.feedItemHashTag = resultItem["hashTag"].stringValue
                feedServiceItem.feedItemCategoryId = resultItem["categoryId"].stringValue
                feedServiceItem.feedItemCoverImage = resultItem["cover"].stringValue
                feedServiceItem.feedItemEventDetails = resultItem["eventDetails"].stringValue
                feedServiceItem.feedItemStartDateTime = NSDate.getDateFromFormmatedString(resultItem["startDateTime"].stringValue)
                feedServiceItem.feedItemEndDateTime = NSDate.getDateFromFormmatedString(resultItem["endDateTime"].stringValue)
                feedServiceItem.feedItemLink = resultItem["link"].stringValue
                feedServiceItem.feedItemEventLikes = resultItem["eventLike"].intValue
                feedServiceItem.feedItemCreatedAt = NSDate.getDateFromFormmatedString(resultItem["createdAt"].stringValue)
                feedServiceItem.feedItemUpdatedAt = NSDate.getDateFromFormmatedString(resultItem["updatedAt"].stringValue)
                
                feedServiceItem.feedItemFriendsCount = resultItem["going"].dictionaryValue["friendsCount"]?.intValue
                feedServiceItem.feedItemOtherCount = resultItem["going"].dictionaryValue["othersCount"]?.intValue
                
                feedServiceItem.feedItemCanEdit = resultItem["canEdit"].boolValue
                
                for avatar in (resultItem["going"].dictionaryValue["avatars"]?.arrayValue)! {
                    feedServiceItem.feedItemAvatars.append(avatar["avatar"].stringValue)
                }
                
                
                // '2015-04-13 12:30:00' (UTC +0)
                
                returnValue.append(feedServiceItem)
                
                
            }
        }
        complection(results:returnValue, error:nil)

        
    }
    
    
   // func sendRequ

}
