//
//  FeedItemServiceModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

enum FeedPrivacy: String {
    case INVITE_ONLY = "InviteOnly"
    case INVITED_GUESTS_FRIENDS = "InvitedGuestsAndFriends"
    case OPEN_INVITE = "OpenInvite"
    case PUBLIC = "Public"
}

class FeedItemServiceModel: NSObject {
    
    var feedItemId:String?
    var feedItemTitle:String?
    var feedItemLongitude:Double?
    var feedItemLattitude:Double?
    var feedItemCity:String?
    var feedItemPrivacy:FeedPrivacy?
    var feedItemAddress:String?
    var feedItemHashTag:String?
    var feedItemCategoryId:String?
    var feedItemCoverImage:String?
    var feedItemEventDetails:String?
    var feedItemStartDateTime:NSDate?
    var feedItemEndDateTime:NSDate?
    var feedItemLink:String?
    var feedItemEventLikes:Int?
    var feedItemCreatedAt:NSDate?
    var feedItemUpdatedAt:NSDate?
    
    var feedItemAvatars:[String] = []
    
    var feedItemFriendsCount:Int?
    var feedItemOtherCount:Int?
    
    var feedItemCanEdit:Bool?
    
    var error:String?
    

}
 