//
//  FeedDataProvider.swift
//  KickCity
//
//  Created by Nabiullin Anton on 11/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

protocol FeedDataProviderDelegate:class {
    func didLoadData()
}

class FeedDataProvider: NSObject {
    
    weak var delegate:FeedDataProviderDelegate?
    
    var allEventsData:[FeedItemViewModel] = []
    var nowEvents:[FeedItemViewModel] = []
    var nearByEvents:[FeedItemViewModel] = []
    var friendsEvents:[FeedItemViewModel] = []
    var searchResultEvents:[FeedItemViewModel] = []
    
    var activeSourceIndex:Int = 0
    
    func loadData(text:String = "") {
        //Think about where to store source index
        switch activeSourceIndex {
        case 0:
            self.getAllEvents()
            break
        case 1:
            self.getNowEvents()
            break
        case 2:
            self.getNearEvents()
            break
        case 3:
            self.getFriendsEvents()
            break
        case 4:
            self.getSearchEvents(text)
            break
        default:
            break
        }

        
    }
    
    private func getFriendsEvents(){
        KickCityServices.sharedInstance.feedService.requestFriendsEvents(nil, offset: friendsEvents.count) { (results, error) -> Void in
            
            if results != nil{
                for result in results! {
                    self.friendsEvents.append(FeedItemViewModel(fromServiceModel: result))
                }
                self.delegate?.didLoadData()
            }else{
                //TODO:Handle Error
            }
            
        }
    }
    
    private func getNearEvents(){
        KickCityServices.sharedInstance.feedService.requestNearEvents(nil, offset: nearByEvents.count) { (results, error) -> Void in
            
            if results != nil{
                for result in results! {
                    self.nearByEvents.append(FeedItemViewModel(fromServiceModel: result))
                }
                self.delegate?.didLoadData()
            }else{
                //TODO:Handle Error
            }
            
        }
    }
    
    private func getNowEvents(){
        KickCityServices.sharedInstance.feedService.requestNowEvents(nil, offset: nowEvents.count) { (results, error) -> Void in
            
            if results != nil{
                for result in results! {
                    self.nowEvents.append(FeedItemViewModel(fromServiceModel: result))
                }
                self.delegate?.didLoadData()
            }else{
                //TODO:Handle Error
            }
            
        }
    }
    
    private func getAllEvents(){
        KickCityServices.sharedInstance.feedService.requestAllEvents(nil, offset: allEventsData.count) { (results, error) -> Void in
            
            if results != nil{
                for result in results! {
                    self.allEventsData.append(FeedItemViewModel(fromServiceModel: result))
                }
                self.delegate?.didLoadData()
            }else{
                //TODO:Handle Error
            }
            
        }
    }
    
    func getSearchEvents(searchText:String) {
        KickCityServices.sharedInstance.feedService.requestFindEvents(searchText, userId: nil, offset: searchResultEvents.count) { (results, error) -> Void in
            if results != nil{
                for result in results! {
                    self.searchResultEvents.append(FeedItemViewModel(fromServiceModel: result))
                }
                self.delegate?.didLoadData()
            }else{
                //TODO:Handle Error
            }

        }
    }
    
    func countOfObjects()->Int {
        
        switch activeSourceIndex {
        case 0:
            return allEventsData.count
        case 1:
            return nowEvents.count
            
        case 2:
            return self.nearByEvents.count
            
        case 3:
            return self.friendsEvents.count
        case 4:
            return self.searchResultEvents.count
        default:
            break
        }
        return allEventsData.count
    }
    
    func getEventForIndex(index:Int) -> FeedItemViewModel {
        
        switch activeSourceIndex {
        case 0:
            return allEventsData[index]
        case 1:
            return nowEvents[index]
            
        case 2:
            return self.nearByEvents[index]
            
        case 3:
            return self.friendsEvents[index]
        case 4:
            return self.searchResultEvents[index]
        default:
            break
        }
        return allEventsData[index]
        
    }
    
    func setActiveSourceIndexValue(index:Int){
        self.activeSourceIndex = index
    }
    
    func getAllData() -> [FeedItemViewModel] {
        return self.allEventsData
    }
    

}
