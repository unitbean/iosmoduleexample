//
//  FeedViewController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import TLYShyNavBar
import PKHUD


class FeedViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UIScrollViewDelegate, FeedDataProviderDelegate, SegmentControllDelegate, UISearchResultsUpdating, UISearchControllerDelegate {


    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var eventsTableView: UITableView!
    private var dataProvider:FeedDataProvider = FeedDataProvider()
    
    var feedSegmentControll = FeedSegmentControll()
    //NSBundle.mainBundle().loadNibNamed(FeedSegmentControll.FeedFeedSegmentControllXibName, owner: self, options: nil).first as? FeedSegmentControll
    
    var resultSearchController = UISearchController()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       //self.definesPresentationContext = true
        
        eventsTableView.delegate = self
        eventsTableView.dataSource = self
        
        /* Library code */
        
        let topSegmentView = UIView(frame: CGRectMake(0, 0, CGRectGetWidth(self.view.bounds), 50))
        topSegmentView.backgroundColor = UIColor.kckCeruleanColor()
        
        feedSegmentControll = (NSBundle.mainBundle().loadNibNamed(FeedSegmentControll.FeedFeedSegmentControllXibName, owner: self, options: nil).first as? FeedSegmentControll)!
  
        let textFieldInsideSearchBar = searchBar.valueForKey("searchField") as? UITextField
        textFieldInsideSearchBar?.textColor = UIColor.whiteColor()
  
            feedSegmentControll.layer.frame = topSegmentView.frame
            feedSegmentControll.delegate = self
            topSegmentView.addSubview(feedSegmentControll)
      //  }

        dataProvider.delegate = self
        
        self.shyNavBarManager.scrollView = self.eventsTableView
        self.shyNavBarManager.extensionView = topSegmentView
         self.shyNavBarManager.extensionView.bringSubviewToFront(self.view)
       // self.navigationController?.navigationBar.bringSubviewToFront(self.view)
         self.shyNavBarManager.stickyNavigationBar = true
        
        self.navigationController?.navigationBar.tintColor = UIColor.kckCeruleanColor()
        self.navigationController?.navigationBar.translucent = false
        self.navigationController?.view.backgroundColor = UIColor.kckCeruleanColor()

        self.navigationController?.navigationBar.backgroundColor = UIColor.kckCeruleanColor()
        
        
        eventsTableView.registerNib(UINib(nibName: FeedItemTableViewCell.cellXib, bundle: nil), forCellReuseIdentifier: FeedItemTableViewCell.cellID)
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        dataProvider.loadData()
        
        
        
        self.resultSearchController = ({
            
            let blur:UIBlurEffect = UIBlurEffect(style: UIBlurEffectStyle.Dark)
            let effectView = UIVisualEffectView(effect: blur)
           
            
            let controller = UISearchController(searchResultsController: nil)
            controller.searchResultsUpdater = self
            controller.dimsBackgroundDuringPresentation = true
        
            controller.searchBar.sizeToFit()
            controller.searchBar.barTintColor = UIColor.kckCeruleanColor()
            
             effectView.frame = controller.view.frame
            
            controller.view.addSubview(effectView)
            
           // controller.searchBar = searchBar
            self.edgesForExtendedLayout = UIRectEdge.Top
          //  controller.searchBar.frame = (self.navigationController?.navigationBar.frame)!
            self.eventsTableView.tableHeaderView = controller.searchBar
            
           
            

            
            return controller
        })()
        //self.resultSearchController.r
        self.resultSearchController.delegate = self
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: .Default)

        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
        //MARK: - UITableView Delegates
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(FeedItemTableViewCell.cellID, forIndexPath: indexPath) as! FeedItemTableViewCell
        
        cell.customizeCell(dataProvider.getEventForIndex(indexPath.row))
        
        return cell
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.resultSearchController.active {
            return self.dataProvider.countOfObjects()
        }
        return self.dataProvider.countOfObjects()
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 280
    }
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == self.dataProvider.countOfObjects() - 2 {

            self.dataProvider.loadData()
        }
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        FeedRouting.showEventSingle(self.dataProvider.getEventForIndex(indexPath.row), fromvc: self)
    }
    
    //MARK: - FeedDataProviderDelegate
    func didLoadData() {
        PKHUD.sharedHUD.hide(afterDelay: 2.0)
        self.eventsTableView.reloadData()
    }
    //MARK: - SegmentControllDelegate
    func segmentControllValueChanged(sender:FeedSegmentControll, selected: Int) {
        self.eventsTableView.setContentOffset(eventsTableView.contentOffset, animated: false)
        self.dataProvider.setActiveSourceIndexValue(selected)
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        
        self.dataProvider.loadData()
        
        
    }
    
    //MARK: - ScrollViewDelegate
    func scrollViewWillBeginDragging(scrollView: UIScrollView) {
        feedSegmentControll.needsUpdateConstraints()
    }
    func scrollViewDidScroll(scrollView: UIScrollView) {
        feedSegmentControll.needsUpdateConstraints()
    }
    func scrollViewDidEndDragging(scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        feedSegmentControll.needsUpdateConstraints()
    }
    func scrollViewDidEndDecelerating(scrollView: UIScrollView) {
        feedSegmentControll.needsUpdateConstraints()
    }
    func scrollViewWillEndDragging(scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        feedSegmentControll.needsUpdateConstraints()
    }
    
    //MARK: - UISearchResultsUpdating
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        self.dataProvider.setActiveSourceIndexValue(4)
        self.dataProvider.loadData(searchController.searchBar.text!)
    }
    
    //MARK: - UISearchControllerDelegate
    func willDismissSearchController(searchController: UISearchController) {
        self.dataProvider.setActiveSourceIndexValue(self.feedSegmentControll.selectedPosition)
    }
    func willPresentSearchController(searchController: UISearchController) {
        self.dataProvider.setActiveSourceIndexValue(4)
        self.eventsTableView.reloadData()
    }

    @IBAction func showFeedOnMapTouchUpInside(sender: AnyObject) {
        FeedRouting.showFeedOnMap(self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
