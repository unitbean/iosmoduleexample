//
//  FeedRouting.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class FeedRouting: NSObject {
    
    static func showEventSingle<T:UIViewController>(viewModel:FeedItemViewModel, fromvc:T) {
        let navvc = UIStoryboard(name: "EventSingle", bundle: nil).instantiateInitialViewController() as! UINavigationController
        let destVC = navvc.viewControllers[0] as! EventSingleViewController
        //let eventSingleVC = destvc.viewControllers[0] as! EventSingleViewController
        destVC.eventViewModel = viewModel

        fromvc.presentViewController(navvc, animated: true, completion: nil)
    }
    
    static func showFeedOnMap<T:UIViewController>( fromvc:T) {
        let navvc = UIStoryboard(name: "FeedOnMap", bundle: nil).instantiateInitialViewController() as! UINavigationController
        navvc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        fromvc.presentViewController(navvc, animated: true, completion: nil)
    }


}
