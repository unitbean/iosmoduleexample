//
//  SegmentControll.swift
//  KickCity
//
//  Created by Nabiullin Anton on 11/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
protocol SegmentControllDelegate:class {
    func segmentControllValueChanged(sender:FeedSegmentControll, selected:Int);
}

class FeedSegmentControll: UIControl {
    
    static let FeedFeedSegmentControllXibName = "FeedSegmentControll"

    @IBOutlet weak var chooseView: UIView!
    @IBOutlet weak var allEventsButton: UIButton!
    @IBOutlet weak var segmentControllNowButton: UIButton!
    @IBOutlet weak var segmentControllNearbyButton: UIButton!
    @IBOutlet weak var segmentControllFriendsButton: UIButton!
    
    
    
     var delegate:SegmentControllDelegate?
    
    var selectedPosition:Int = 0
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override func needsUpdateConstraints() -> Bool {
        switch selectedPosition {
        case 0:
            self.chooseView.center.x = self.allEventsButton.center.x
            break
        case 1:
            self.chooseView.center.x = self.segmentControllNowButton.center.x
            break
        case 2:
            self.chooseView.center.x = self.segmentControllNearbyButton.center.x
            break
        case 3:
            self.chooseView.center.x = self.segmentControllFriendsButton.center.x
            break
        default:
            self.chooseView.center.x = self.segmentControllFriendsButton.center.x
            break
        }
   //     self.chooseView.center.x = self.segmentControllFriendsButton.center.x
        return true
    }
    
    
    required init(coder: NSCoder) {
        super.init(coder: coder)!
     
    }
    
    override func didMoveToWindow() {
        self.setActiveColorButton()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    override func awakeFromNib() {
        print("fdsfds")
        self.setActiveColorButton()
    }
    
    override func drawRect(rect: CGRect) {

        
        self.chooseView.layer.cornerRadius =  self.chooseView.layer.frame.height / 2
        self.chooseView.layer.masksToBounds = true
      
    }
    
    private func setActiveColorButton() {
        switch selectedPosition{
        case 0:
            allEventsButton.setTitleColor(UIColor.kckCeruleanColor(), forState: UIControlState.Normal)
            break
        case 1:
            segmentControllNowButton.setTitleColor(UIColor.kckCeruleanColor(), forState: UIControlState.Normal)
            break
        case 2:
            segmentControllNearbyButton.setTitleColor(UIColor.kckCeruleanColor(), forState: UIControlState.Normal)
            break
        case 3:
            segmentControllFriendsButton.setTitleColor(UIColor.kckCeruleanColor(), forState: UIControlState.Normal)
            break
        default:
            break
            
            
            
        }
    }
    
    private func setPreviusColorNormal(nextValue:Int) {
        if nextValue == selectedPosition {
            return
        }
        switch selectedPosition{
            case 0:
                allEventsButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                break
            case 1:
                segmentControllNowButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            break
            case 2:
                segmentControllNearbyButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            break
            case 3:
                segmentControllFriendsButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            break
            default:
                break
            
            
            
        }
    }
    

    
    @IBAction func friendsButtonPressed(sender: AnyObject) {
        setPreviusColorNormal(3)
        selectedPosition = 3
        UIView.animateWithDuration(0.5, animations: {
            self.setActiveColorButton()
            self.chooseView.center.x = self.segmentControllFriendsButton.center.x
        })
        delegate?.segmentControllValueChanged(self, selected:selectedPosition)
    }

    @IBAction func nearByEventsPressed(sender: AnyObject) {
        setPreviusColorNormal(2)
        selectedPosition = 2
 
        UIView.animateWithDuration(0.5, animations: {
            self.setActiveColorButton()
            self.chooseView.center.x = self.segmentControllNearbyButton.center.x
            
        })
        delegate?.segmentControllValueChanged(self, selected:selectedPosition)
    }

    @IBAction func allEventsPressed(sender: AnyObject) {
        setPreviusColorNormal(0)
        selectedPosition = 0

        UIView.animateWithDuration(0.5, animations: {
            self.setActiveColorButton()
            self.chooseView.center.x = self.allEventsButton.center.x
        })
        delegate?.segmentControllValueChanged(self, selected:selectedPosition)
    }
    
    @IBAction func nowEventsPressed(sender: AnyObject) {
        setPreviusColorNormal(1)
        selectedPosition = 1
        UIView.animateWithDuration(0.5, animations: {
            self.setActiveColorButton()
            self.chooseView.center.x = self.segmentControllNowButton.center.x
        })
        delegate?.segmentControllValueChanged(self, selected:selectedPosition)
    }

}
