//
//  FeedItemViewModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import CoreLocation
//import D

enum FeedItemType {
    case NEAR
    case FRIEND
    case SOON
}

class FeedItemViewModel {
    
    var feedItemType:FeedItemType?
    var feedItemId:String?
    var feedItemTitle:String?
    var feedItemPlaceAndTime:String?
    var feedItemTime:NSDate?
    var feedItemPeopleCount:String?
    var feedItemFriendsCount:String?
    var feedItemImage:String?
    var userFrontImage:String?
    var userBackImage:String?
    var frienImage:String?
    var feedItemIconText:String?
    
    //MARK: - single events variabels
    var feedItemDescription:String?
    var feedItemLink:String?
    var feedItemMonth:String?
    var feedItemDay:String?
    
    //MARK: - feedMap
    var longitude:Double?
    var lattitude:Double?
    
    init (){
        
    }
    
    
    init(fromServiceModel:FeedItemServiceModel) {
        feedItemTitle = fromServiceModel.feedItemTitle
        feedItemId = fromServiceModel.feedItemId
        feedItemPlaceAndTime = fromServiceModel.feedItemAddress! + " " + (fromServiceModel.feedItemStartDateTime?.getFeedDateFormat())!
        //feedItemTime = fromServiceModel.feedItemStartDateTime
        
        if  fromServiceModel.feedItemFriendsCount == 0 {
            feedItemFriendsCount = ""
            
            if  fromServiceModel.feedItemOtherCount == 0 {
                feedItemPeopleCount = ""
            }else{
                
                feedItemPeopleCount = "\(fromServiceModel.feedItemOtherCount!) other going"//
            }
            
        }else{
            feedItemFriendsCount = "\(fromServiceModel.feedItemFriendsCount!) friends"//
            
            if  fromServiceModel.feedItemOtherCount == 0 {
                feedItemPeopleCount = ""
            }else{
                
                feedItemPeopleCount = "& \(fromServiceModel.feedItemOtherCount!) other going"//
            }

            
        }

        feedItemImage = KickCityConstants.FILE_URL + fromServiceModel.feedItemCoverImage!
        if fromServiceModel.feedItemAvatars.count > 0 {
            for var i = 0; i<fromServiceModel.feedItemAvatars.count ; ++i {
                if i == 0 {
                    userFrontImage = KickCityConstants.FILE_URL + fromServiceModel.feedItemAvatars[i]
                }else{
                    userBackImage = KickCityConstants.FILE_URL + fromServiceModel.feedItemAvatars[i]
                }
            }
        }
        
        let sysLatitude = UserDefaultsUtils.getLattitude()
        let sysLongitude = UserDefaultsUtils.getLongitude()
        let phoneLocation = CLLocation(latitude: sysLatitude!, longitude: sysLongitude!)
        
    
        
        let eventLocation = CLLocation(latitude: fromServiceModel.feedItemLattitude!, longitude: fromServiceModel.feedItemLongitude!)
        
        let distance = phoneLocation.distanceFromLocation(eventLocation)
        
        feedItemType = FeedItemType.NEAR
        
        //let nf = DecimalFormat("#.####")
        if distance > 1000 {
            feedItemIconText = " \(Int(distance / 1000)) km"
        }else{
             feedItemIconText = " \(Int(distance)) m"
        }
        
        
        
        if ((fromServiceModel.feedItemStartDateTime?.isToday()) == true) {
            feedItemType = FeedItemType.SOON
            
            if fromServiceModel.feedItemStartDateTime?.hoursLaterThan(NSDate()) < 1 {
                feedItemIconText = String( fromServiceModel.feedItemStartDateTime?.minutesLaterThan(NSDate()) )
            }else{
                feedItemIconText = String( fromServiceModel.feedItemStartDateTime?.hoursLaterThan(NSDate()) )
            }
            
            
        }

        feedItemDescription = fromServiceModel.feedItemEventDetails
        feedItemLink = fromServiceModel.feedItemLink
        feedItemMonth = fromServiceModel.feedItemStartDateTime?.getMonthName()
        feedItemDay = fromServiceModel.feedItemStartDateTime?.getDayStringNumber()
        
        longitude = fromServiceModel.feedItemLongitude
        lattitude = fromServiceModel.feedItemLattitude
        
        
    }
    

}
