//
//  FeedItemTableViewCell.swift
//  KickCity
//
//  Created by Nabiullin Anton on 10/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import SDWebImage

class FeedItemTableViewCell: UITableViewCell {

    @IBOutlet weak var feedCellImageBG: UIImageView!
    @IBOutlet weak var feedCellEventIcon: UIImageView!
    @IBOutlet weak var feedCellIconText: UILabel!
    @IBOutlet weak var feedCellEventTitle: UILabel!
    @IBOutlet weak var feedCellEventPlace: UILabel!
    @IBOutlet weak var feedCellOtherGoingLabel: UILabel!
    @IBOutlet weak var feedCellFriendsGoingLabel: UILabel!
    @IBOutlet weak var feedCellUserFrontImage: UIImageView!
    @IBOutlet weak var feedCellUserBackImage: UIImageView!
    @IBOutlet weak var feedCellOverlayView: UIView!
    
    
    static let cellXib = "FeedItem"
    static let cellID = "FeedItemID"
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        feedCellEventIcon.layer.cornerRadius = feedCellEventIcon.layer.frame.height / 2
        //feedCellEventIcon.layer.borderWidth = 1
       // feedCellEventIcon.layer.borderColor = UIColor.kckWhiteColor().CGColor
        feedCellEventIcon.layer.masksToBounds = true
        

        


        
        feedCellOverlayView.layer.cornerRadius = 4
        feedCellImageBG.layer.cornerRadius = 4
        feedCellOverlayView.layer.masksToBounds = true
        feedCellImageBG.layer.masksToBounds = true
        
        
        
        
    }
    
    func customizeCell(feedItem:FeedItemViewModel){
        
        if feedItem.feedItemType == FeedItemType.NEAR{

            self.feedCellEventIcon.image = UIImage(named: "mapIcon")
            
        }else if feedItem.feedItemType == FeedItemType.SOON {
            self.feedCellEventIcon.image = UIImage(named: "timefeedIcon")
            
        }else{
            self.feedCellEventIcon.sd_setImageWithURL(NSURL(string: feedItem.frienImage!))
        }
        self.feedCellImageBG.sd_setImageWithURL(NSURL(string: feedItem.feedItemImage!))
        self.feedCellIconText.text = feedItem.feedItemIconText
        self.feedCellEventTitle.text = feedItem.feedItemTitle
        self.feedCellEventPlace.text = feedItem.feedItemPlaceAndTime
        self.feedCellOtherGoingLabel.text = feedItem.feedItemPeopleCount
        self.feedCellFriendsGoingLabel.text = feedItem.feedItemFriendsCount
        
        if  feedItem.userFrontImage != nil {
            feedCellUserFrontImage.hidden = false
            
            feedCellUserFrontImage.layer.cornerRadius = feedCellUserFrontImage.layer.frame.height / 2
            feedCellUserFrontImage.layer.borderWidth = 1
            feedCellUserFrontImage.layer.borderColor = UIColor.kckWhiteColor().CGColor
            feedCellUserFrontImage.layer.masksToBounds = true

            self.feedCellUserFrontImage.sd_setImageWithURL(NSURL(string: feedItem.userFrontImage!))
        }else{
            feedCellUserFrontImage.hidden = true
        }
        if  feedItem.userBackImage != nil {
            feedCellUserBackImage.hidden = false
            
            feedCellUserBackImage.layer.borderColor = UIColor.kckWhiteColor().CGColor
            feedCellUserBackImage.layer.masksToBounds = true
            feedCellUserBackImage.layer.cornerRadius = feedCellUserBackImage.layer.frame.height / 2
            feedCellUserBackImage.layer.borderWidth = 1
            self.feedCellUserBackImage.sd_setImageWithURL(NSURL(string: feedItem.userBackImage!))
        }else{
            feedCellUserBackImage.hidden = true
        }
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
