//
//  EventSingleServices.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

typealias GetCommentsForEventComplection = (results: [EventSingleServiceModel]?, error:String?) -> Void

class EventSingleServices: KickCityApiService {
    
    let commentsLimit = 20
    
    func getCommentsForEvent(eventId:String, offset:Int, complection: GetCommentsForEventComplection) {
        let url = self.url("/db/event/comment/" + eventId)
        
        var params:[String:AnyObject] = [String:AnyObject]()
        
        params["limit"] = commentsLimit
        params["offset"] = offset

        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
            self.parseCommentObject(results, complection: complection)

        }
        

        
    }
    
    private func parseCommentObject(data:JSON?, complection:GetCommentsForEventComplection) {
        
        if data == nil ||  data!["error"] != nil {
            complection(results:nil, error:"Error while getting data from api")
            return
        }
        print(data)
        var returnValue:[EventSingleServiceModel] = []
        for resultItem in data!["rows"].arrayValue {
            print("////////HERE////\(resultItem)")
            let commentItem:EventSingleServiceModel = EventSingleServiceModel()

            commentItem.commentID = resultItem["id"].stringValue
            commentItem.commentText = resultItem["text"].stringValue
            commentItem.userId = resultItem["userId"].stringValue
            commentItem.userName = resultItem["user"].dictionaryValue["name"]!.stringValue
            commentItem.avatar = resultItem["user"].dictionaryValue["avatar"]!.stringValue
            commentItem.sex = resultItem["user"].dictionaryValue["sex"]!.stringValue
            commentItem.mentionUserId = resultItem["mentionUserId"].stringValue
            commentItem.createdAt = NSDate.getDateFromFormmatedString(resultItem["createdAt"].stringValue)
            commentItem.updatedAt = NSDate.getDateFromFormmatedString(resultItem["updatedAt"].stringValue)
            
            returnValue.append(commentItem)
            
        }
        complection(results:returnValue, error:nil)
        
        
    }

    
    
    

}
