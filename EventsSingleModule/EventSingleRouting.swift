//
//  EventSingleRouting.swift
//  KickCity
//
//  Created by Nabiullin Anton on 20/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class EventSingleRouting: NSObject {
    
    static func fromEventSingleToDescription<T:UIViewController>(viewModel:FeedItemViewModel, fromvc:T) {
        let navvc = UIStoryboard(name: "EventSingle", bundle: nil).instantiateViewControllerWithIdentifier("EventMoreInformation") as! UINavigationController
        let destVC = navvc.viewControllers[0] as! EventMoreInformationViewController

        destVC.feedItem = viewModel
        navvc.modalTransitionStyle = UIModalTransitionStyle.FlipHorizontal
        fromvc.presentViewController(navvc, animated: true, completion: nil)

    }


}
