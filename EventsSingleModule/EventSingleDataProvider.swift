//
//  EventSingleDataProvider.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

protocol EventSingleDataProviderDelegate:class {
    
    func eventSingleDataProviderLoadCommentFaildWithError(error:String)
    func eventSingleDataProviderDidLoadComments(  )
    
}

class EventSingleDataProvider: NSObject {
    
    var eventViewModel:FeedItemViewModel?
    
    private var commentsData:[CommentItemViewModel] = []

    weak var delegate:EventSingleDataProviderDelegate?
    
    func loadEventComments() {
        KickCityServices.sharedInstance.commentService.getCommentsForEvent(eventViewModel!.feedItemId!, offset: commentsData.count) { (results, error) -> Void in
            if error != nil {
                self.delegate?.eventSingleDataProviderLoadCommentFaildWithError("Problem with load comments")
            } else {
               // var commentsData:[CommentItemViewModel] = []
                for resultItem in results! {
                    let commentSingleViewModel = CommentItemViewModel(fromServiceModel: resultItem)
                    self.commentsData.append(commentSingleViewModel)
                }
                
                self.delegate?.eventSingleDataProviderDidLoadComments()
            }
        }
    }
    func countOnComments()->Int {
        return commentsData.count
    }
    func getCommentForIndex(index:Int) -> CommentItemViewModel {
        return commentsData[index]
    }
}
