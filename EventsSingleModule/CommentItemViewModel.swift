//
//  CommentItemViewModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class CommentItemViewModel {
    
    var commentText:String?
    var postedTimeAgo:String?
    var commentId:String?
    var authorId:String?
    var authorImage:String?
    var authorName:String?
    
    init(fromServiceModel:EventSingleServiceModel) {
        self.commentText = fromServiceModel.commentText
        self.commentId = fromServiceModel.commentID
        self.authorId = fromServiceModel.userId
        self.authorImage = KickCityConstants.FILE_URL + fromServiceModel.avatar!
        self.authorName = fromServiceModel.userName
        self.postedTimeAgo = fromServiceModel.updatedAt?.timeAgo
    }

}
