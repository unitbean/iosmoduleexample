//
//  EventSingleViewController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import PKHUD

class EventSingleViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, EventSingleDataProviderDelegate {
    
    var eventId:String?
    var eventViewModel:FeedItemViewModel?
    
    private var dataProvider:EventSingleDataProvider = EventSingleDataProvider()

    @IBOutlet weak var eventDescriptionTableView: UITableView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
         self.edgesForExtendedLayout = UIRectEdge.None


        // Do any additional setup after loading the view.
        eventDescriptionTableView.delegate = self
        eventDescriptionTableView.dataSource = self
        eventDescriptionTableView.registerNib(UINib(nibName: EventHeaderTableViewCell.cellXib, bundle: nil), forCellReuseIdentifier: EventHeaderTableViewCell.cellID)
        eventDescriptionTableView.registerNib(UINib(nibName: EventCommentTableViewCell.cellXib, bundle: nil), forCellReuseIdentifier: EventCommentTableViewCell.cellID)
       // eventDescriptionTableView.rowHeight = UITableViewAutomaticDimension
        eventDescriptionTableView.estimatedRowHeight = 54
        eventDescriptionTableView.tableFooterView = UIView()

        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        dataProvider.eventViewModel = eventViewModel
        dataProvider.delegate = self
        dataProvider.loadEventComments()
        
        self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clearColor())
        self.navigationController?.navigationBar.shadowImage = UIImage()
        
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.lt_reset()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - UITableViewDelegates
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
       // return UITableViewCell()
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(EventHeaderTableViewCell.cellID, forIndexPath: indexPath) as! EventHeaderTableViewCell
            cell.customizeCell(eventViewModel!)
            return cell
        }
        let cell = tableView.dequeueReusableCellWithIdentifier(EventCommentTableViewCell.cellID, forIndexPath: indexPath) as! EventCommentTableViewCell
        cell.customizeCell(self.dataProvider.getCommentForIndex(indexPath.row - 1))
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.dataProvider.countOnComments() + 1
    }
    

    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if indexPath.row == 0 {
            return tableView.frame.size.height
        }else{
            tableView.rowHeight = UITableViewAutomaticDimension
            return 61
        }
        
    }
    
    //MARK: - EventSingleDataProviderDelegate
    func eventSingleDataProviderDidLoadComments() {
        
        PKHUD.sharedHUD.hide(afterDelay: 0.3)
        self.eventDescriptionTableView.reloadData()
        
    }
    func eventSingleDataProviderLoadCommentFaildWithError(error: String) {
        PKHUD.sharedHUD.hide(afterDelay: 0.3)
        KickCityAlertUtils.showSimpleAlertWithTitle(error, fromVC: self)
    }
    
    
    //MARK: - Scroll
    func scrollViewDidScroll(scrollView: UIScrollView) {
        let color = UIColor.kckCeruleanColor()
        let offsetY = scrollView.contentOffset.y
        if offsetY > 50 {
            let alpha =  min(1, 1 - ((50 + 64 - offsetY) / 64))
            self.navigationController?.navigationBar.lt_setBackgroundColor(color.colorWithAlphaComponent(alpha))
        }else{
            self.navigationController?.navigationBar.lt_setBackgroundColor(color.colorWithAlphaComponent(0))
        }
    }
    
    //MARK: - Events
    @IBAction func closeTabBarTouchUpInside(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func aboutTouchUpInside(sender: AnyObject) {
        EventSingleRouting.fromEventSingleToDescription(self.eventViewModel!, fromvc: self)
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
