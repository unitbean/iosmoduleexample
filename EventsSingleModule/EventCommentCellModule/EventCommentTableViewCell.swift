//
//  EventCommentTableViewCell.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import SDWebImage

class EventCommentTableViewCell: UITableViewCell {
    
    static let cellID = "EventCommentTableViewCell"
    static let cellXib = "EventCommentCell"
    
    
    @IBOutlet weak var eventCommentAuthorImageViewCell: KickCityUserGoingImageView!
    
    @IBOutlet weak var eventCommentAuthorName: UILabel!
    @IBOutlet weak var eventCommentText: UILabel!
    @IBOutlet weak var eventCommentDate: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func customizeCell(item:CommentItemViewModel) {
        self.eventCommentAuthorImageViewCell.sd_setImageWithURL( NSURL( string:item.authorImage! ) )
        self.eventCommentAuthorName.text = item.authorName
        self.eventCommentText.text = item.commentText
        self.eventCommentDate.text = item.postedTimeAgo
    }

}
