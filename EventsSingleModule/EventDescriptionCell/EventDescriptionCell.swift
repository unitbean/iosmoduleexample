//
//  EventDescriptionCell.swift
//  KickCity
//
//  Created by Nabiullin Anton on 19/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import GoogleMaps



class EventDescriptionCell: UITableViewCell {
    
    static let cellID = "EVENT_DESCRIPTION_CELL"
    static let cellXib = "EventDescriptionCell"

    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var bgImageView: UIImageView!
    @IBOutlet weak var eventMonthLabel: UILabel!
    @IBOutlet weak var eventDateLabel: UILabel!
    @IBOutlet weak var eventTitleLabel: UILabel!
    
    @IBOutlet weak var eventDescriptionLabel: UILabel!
    
    @IBOutlet weak var eventLinkButton: UIButton!
    
    @IBOutlet weak var calendarBgView: UIView!
    @IBOutlet weak var mapBgView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        calendarBgView.layer.cornerRadius = 4
        calendarBgView.layer.masksToBounds = true
        
        mapBgView.layer.cornerRadius = 4
        mapBgView.layer.masksToBounds = true
        
        eventDescriptionLabel.sizeToFit()
        
        
        
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func customizeCell(model:FeedItemViewModel) {
        
        let camera = GMSCameraPosition.cameraWithLatitude(-33.868,
            longitude:151.2086, zoom:10)
        let mapView = GMSMapView.mapWithFrame(self.mapView.bounds, camera:camera)
        
        let marker = GMSMarker()
        marker.position = camera.target
        marker.snippet = "Hello World"
        marker.appearAnimation = kGMSMarkerAnimationPop
        marker.map = mapView
        
        
        self.mapView.addSubview(mapView)
        
        
        eventTitleLabel.text = model.feedItemTitle
        bgImageView.sd_setImageWithURL(NSURL(string: model.feedItemImage!))
        eventDescriptionLabel.text = model.feedItemDescription
        eventLinkButton.setTitle(model.feedItemLink, forState: UIControlState.Normal)

        eventDateLabel.text = model.feedItemDay
        eventMonthLabel.text = model.feedItemMonth
    }
    
    @IBAction func addToCalendarTouchUpInside(sender: AnyObject) {
    }

    @IBAction func showOnMapTouchUpInside(sender: AnyObject) {
    }
    
    @IBAction func showLinkTouchUpInside(sender: AnyObject) {
    }
    
}
