//
//  EventSingleServiceModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class EventSingleServiceModel: NSObject {
    
    var commentID:String?
    var commentText:String?
    var createdAt:NSDate?
    var updatedAt:NSDate?
    var userId:String?
    var userName:String?
    var avatar:String?
    var sex:String?
    var mentionUserId:String?

}
