//
//  EventMoreInformationViewController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 19/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class EventMoreInformationViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var eventSingleDescriptionTableView: UITableView!
    
    var feedItem:FeedItemViewModel?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.edgesForExtendedLayout = UIRectEdge.None

        // Do any additional setup after loading the view.
        eventSingleDescriptionTableView.delegate = self
        eventSingleDescriptionTableView.dataSource = self
        
        eventSingleDescriptionTableView.registerNib(UINib(nibName: EventDescriptionCell.cellXib, bundle: nil), forCellReuseIdentifier: EventDescriptionCell.cellID)
        
        eventSingleDescriptionTableView.rowHeight = UITableViewAutomaticDimension
        
        
        self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clearColor())
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        //self.navigationController?.navigationBar.lt_reset()
    }
    
    //MARK: - UITableViewDataSource
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier(EventDescriptionCell.cellID, forIndexPath: indexPath) as!  EventDescriptionCell
        cell.customizeCell(feedItem!)
        return cell
        
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
            return tableView.frame.size.height

        
    }
    
    @IBAction func backButtonTouchUpInside(sender: AnyObject) {
        
        self.dismissViewControllerAnimated(true, completion: nil)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
