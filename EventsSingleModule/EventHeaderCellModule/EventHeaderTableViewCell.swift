//
//  EventHeaderTableViewCell.swift
//  KickCity
//
//  Created by Nabiullin Anton on 15/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import SDWebImage

class EventHeaderTableViewCell: UITableViewCell {
    
    static let cellID = "EventHeaderTableViewCell"
    static let cellXib = "EventHeaderCell"
    

    @IBOutlet weak var eventCellCoverImageView: UIImageView!
    
    @IBOutlet weak var eventCellTitleLabel: UILabel!
    @IBOutlet weak var eventCellAddressPlaceLabel: UILabel!
    
    @IBOutlet weak var eventCellUserFrontImageView: KickCityUserGoingImageView!
    
    @IBOutlet weak var eventCellUserBackImageView: KickCityUserGoingImageView!
    
    @IBOutlet weak var eventCellOtherGoing: UILabel!
    @IBOutlet weak var eventCellFriendsGoing: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    
    
    func customizeCell(feedItem: FeedItemViewModel) {
        
        
        self.eventCellCoverImageView.sd_setImageWithURL(NSURL(string: feedItem.feedItemImage!))
        self.eventCellTitleLabel.text = feedItem.feedItemTitle
        self.eventCellAddressPlaceLabel.text = feedItem.feedItemPlaceAndTime
        self.eventCellOtherGoing.text = feedItem.feedItemPeopleCount
        self.eventCellFriendsGoing.text = feedItem.feedItemFriendsCount
        
        if  feedItem.userFrontImage != nil {
            eventCellUserFrontImageView.hidden = false
            
            
            self.eventCellUserFrontImageView.sd_setImageWithURL(NSURL(string: feedItem.userFrontImage!))
        }else{
            eventCellUserFrontImageView.hidden = true
        }
        if  feedItem.userBackImage != nil {
            eventCellUserBackImageView.hidden = false

            self.eventCellUserBackImageView.sd_setImageWithURL(NSURL(string: feedItem.userBackImage!))
        }else{
            eventCellUserBackImageView.hidden = true
        }

        
        
    }
    
    

    @IBAction func eventCelJoinEventButtonTouchUpInside(sender: AnyObject) {
    }
}
