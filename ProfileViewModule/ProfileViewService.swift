//
//  ProfileViewService.swift
//  KickCity
//
//  Created by Nabiullin Anton on 25/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import SwiftyJSON

typealias GetUserInformation = (results: ProfileViewServiceModel?, error:String?) -> Void

class ProfileViewService: KickCityApiService {
    
    func getUserInformation(userID:String?, complection:GetUserInformation) {
        var params:[String:AnyObject] = [String:AnyObject]()
        if userID != nil {
            params["id"] = userID
            params["all"] = "true"
        }
        let url = self.url("/db/user")
        self.sendRequest(.GET, url: url, params: params) { (results) -> Void in
            
              self.parseProfileObject(results, complection: complection)
            
        }

    }
    
    private func parseProfileObject(data:JSON?, complection:GetUserInformation) {
        
        if data == nil ||  data!["error"] != nil {
            complection(results:nil, error:"Error while getting data from api")
            return
        }
        print(data)
        let returnValue:ProfileViewServiceModel = ProfileViewServiceModel()
        returnValue.profileViewServiceModelId = data!["id"].stringValue
        returnValue.profileViewServiceModelFirstName = data!["firstName"].stringValue
        returnValue.profileViewServiceModelLastName = data!["lastName"].stringValue
        returnValue.profileViewServiceModelEmail = data!["email"].stringValue
        returnValue.profileViewServiceModelJob = data!["job"].stringValue
        returnValue.profileViewServiceModelAvatar = data!["avatar"].stringValue
        returnValue.profileViewServiceModelCity = data!["city"].stringValue
        returnValue.profileViewServiceModelAvatar = data!["avatar"].stringValue
        returnValue.profileViewServiceModelEventsCount = data!["stats"].dictionaryValue["eventsCount"]?.intValue
        returnValue.profileViewServiceModelFollowersCount = data!["stats"].dictionaryValue["followersCount"]?.intValue
        returnValue.profileViewServiceModelFollowingCount = data!["stats"].dictionaryValue["followingCount"]?.intValue
        returnValue.profileViewServiceModelIsAccountPrivate = data!["isAccountPrivate"].boolValue
        returnValue.profileViewServiceModelIsIamFollowUser = data!["isIamFollowUser"].boolValue
        returnValue.profileViewServiceModelIsUserFollowMe = data!["isUserFollowMe"].boolValue
        returnValue.profileViewServiceModelSex = data!["sex"].stringValue
        returnValue.profileViewServiceModelStatus = data!["status"].stringValue
        
        
        complection(results:returnValue, error:nil)
        
        
    }
    
    

}
