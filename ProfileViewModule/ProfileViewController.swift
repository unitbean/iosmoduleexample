//
//  ProfileViewController.swift
//  KickCity
//
//  Created by Nabiullin Anton on 25/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
import PKHUD

class ProfileViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, ProfileViewDataProviderDelegate, ProfileHeaderCellDelegate {

    @IBOutlet weak var profileTableView: UITableView!
    private var dataProvider:ProfileViewDataProvider = ProfileViewDataProvider()
    var userId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
        self.edgesForExtendedLayout = UIRectEdge.None
        
        profileTableView.registerNib(UINib(nibName: ProfileHeaderCell.cellXib, bundle: nil), forCellReuseIdentifier: ProfileHeaderCell.cellID)
        profileTableView.registerNib(UINib(nibName: FeedItemTableViewCell.cellXib, bundle: nil), forCellReuseIdentifier: FeedItemTableViewCell.cellID)
        profileTableView.registerNib(UINib(nibName: SectionSwitcherCell.cellXib, bundle: nil), forCellReuseIdentifier: SectionSwitcherCell.cellID)
        profileTableView.delegate = self
        profileTableView.dataSource = self
        profileTableView.tableFooterView = UIView()
        
        PKHUD.sharedHUD.contentView = PKHUDProgressView()
        PKHUD.sharedHUD.show()
        
        dataProvider.getUserInfoData(userId)
        dataProvider.delegate = self
        
     //   self.hideNavigation()
        self.navigationController?.navigationBar.lt_setBackgroundColor(UIColor.clearColor())
        self.navigationController?.navigationBar.shadowImage = UIImage()

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - UITableViewDelegate
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            let cell = tableView.dequeueReusableCellWithIdentifier(ProfileHeaderCell.cellID, forIndexPath: indexPath) as! ProfileHeaderCell
            cell.customizeCell(dataProvider.getUserInfo())
            cell.delegate = self
            return cell
        }else{
            let cell = tableView.dequeueReusableCellWithIdentifier(FeedItemTableViewCell.cellID, forIndexPath: indexPath) as! FeedItemTableViewCell
            cell.customizeCell(self.dataProvider.getEventsForIndex(indexPath.row))
            return cell
            
        }
        
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0{
            if dataProvider.userInfoLoaded() == true {
                return 1
            }
        }else{
            return dataProvider.getEventsCout()
        }
        return 0
        
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if section == 0 {
            return UIView()
        }else{
            let sectionHeader = tableView.dequeueReusableCellWithIdentifier(SectionSwitcherCell.cellID) as! SectionSwitcherCell
            sectionHeader.profileName = self.dataProvider.getUserInfo().profileViewUserName!
            //sectionHeader.delegate = self
            return sectionHeader
        }
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if section == 0 {
            return 0
        }else{
            return 50
        }
    }
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        if dataProvider.userInfoLoaded() == true {
            return 2
        }else{
            return 1
        }
        
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 338
    }
    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath.row == self.dataProvider.getEventsCout() - 2 {
            
            self.dataProvider.loadMoreEvents(userId)
        }
    }
    
    //MARK: - ProfileViewDataProviderDelegate
    func profileViewDataProviderDidFaildWithError(erro: String) {
        PKHUD.sharedHUD.hide(animated: true, completion: nil)
        KickCityAlertUtils.showSimpleAlertWithTitle(erro, fromVC: self)
    }
    func profileViewDataProviderDidLoadData() {
        PKHUD.sharedHUD.hide(animated: true, completion: nil)
        profileTableView.reloadData()
    }
    
    //MARK: cistomazation
    private func hideNavigation(){
        
       // self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        self.navigationController?.navigationBar.translucent = true
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.view.backgroundColor = UIColor.clearColor()
        self.navigationController?.navigationBar.backgroundColor = UIColor.clearColor()
        
        
    }
    
    //MARK: - ProfileHeaderCellDelegate
    func profileHeaderCellEditProfileTouched() {
        EditProfileRouting.presentEditProfile(dataProvider.getUserInfo(), fromvc: self)


    
    }
    func profileHeaderCellShowFollowers() {
        UserFollwerFollowingRouting.showFollowers(self.dataProvider.getUserInfo(), fromvc: self)
    }
    func profileHeaderCellShowFollowing() {
        UserFollwerFollowingRouting.showFollowing(self.dataProvider.getUserInfo(), fromvc: self)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
