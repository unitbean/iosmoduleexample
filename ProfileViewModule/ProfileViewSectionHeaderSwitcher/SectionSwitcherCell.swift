//
//  SectionSwitcherCell.swift
//  KickCity
//
//  Created by Nabiullin Anton on 29/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
protocol SectionSwitcherCellDelegate:class {
    func sectionSwitcherCellValueChanged(sender:SectionSwitcherCell, selected:Int);
}

class SectionSwitcherCell: UITableViewCell {
    
    static let cellID = "SECTION_SWITCHER"
    static let cellXib = "SectionSwitcher"

    var profileName:String = ""

    @IBOutlet weak var rightButton: UIButton!
    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var selectedView: UIView!
    
     var delegate:SectionSwitcherCellDelegate?
    
    var selectedPosition:Int = 0
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        selectedView.layer.cornerRadius = selectedView.layer.frame.height / 2
        selectedView.layer.masksToBounds = true
        
        rightButton.setTitle("Visited", forState: UIControlState.Normal)
        leftButton.setTitle(profileName + "'s Events", forState: UIControlState.Normal)
    }
    
    
    private func setPreviusColorNormal(nextValue:Int) {
        if nextValue == selectedPosition {
            return
        }
        switch selectedPosition{
        case 0:
            leftButton.setTitleColor(UIColor.kckBlack70Color(), forState: UIControlState.Normal)
            break
        case 1:
            rightButton.setTitleColor(UIColor.kckBlack70Color(), forState: UIControlState.Normal)
            break
        default:
            break
            
            
            
        }
    }
    

    
    private func setActiveColorButton() {
        switch selectedPosition{
        case 0:
            leftButton.setTitleColor(UIColor.kckWhiteColor(), forState: UIControlState.Normal)
            break
        case 1:
            rightButton.setTitleColor(UIColor.kckWhiteColor(), forState: UIControlState.Normal)
        default:
            break
            
            
            
        }
    }
    

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    @IBAction func rightButtonTouchUpInside(sender: AnyObject) {
        
        setPreviusColorNormal(1)
        selectedPosition = 1
        
        UIView.animateWithDuration(0.5, animations: {
            self.setActiveColorButton()
            self.selectedView.center.x = self.rightButton.center.x
        })
        delegate?.sectionSwitcherCellValueChanged(self, selected:selectedPosition)
        
    }
    @IBAction func leftButtonTouchUpInside(sender: AnyObject) {
        setPreviusColorNormal(0)
        selectedPosition = 0
        
        UIView.animateWithDuration(0.5, animations: {
            self.setActiveColorButton()
            self.selectedView.center.x = self.leftButton.center.x
        })
        delegate?.sectionSwitcherCellValueChanged(self, selected:selectedPosition)

    }
}
