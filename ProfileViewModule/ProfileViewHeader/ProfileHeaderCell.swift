//
//  ProfileHeaderCell.swift
//  KickCity
//
//  Created by Nabiullin Anton on 25/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
protocol ProfileHeaderCellDelegate:class{
    func profileHeaderCellEditProfileTouched()
    func profileHeaderCellShowFollowing()
    func profileHeaderCellShowFollowers()
}

class ProfileHeaderCell: UITableViewCell {
    
    static let cellID = "PROFILE_HEADER_CELL"
    static let cellXib = "ProfileHeader"
    
    weak var delegate:ProfileHeaderCellDelegate?

    @IBOutlet weak var profileHeaderCellImageView: UIImageView!
    @IBOutlet weak var profileHeaderCellCityLabel: UILabel!
    @IBOutlet weak var profileHeaderCellAvatarImageView: KickCityUserGoingImageView!
    @IBOutlet weak var profileHeaderCellUserNameLabel: UILabel!
    @IBOutlet weak var profileHeaderCellInterestLabel: UILabel!
    @IBOutlet weak var profileHeaderCellActionButton: KickCityRoundedButton!
    @IBOutlet weak var profileHeaderCellFollowersCount: UILabel!
    @IBOutlet weak var profileHeaderCellFollowingCount: UILabel!
    @IBOutlet weak var profileHeaderCellEventsCount: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func customizeCell(viewModel:ProfileViewItemViewModel) {
        profileHeaderCellImageView.sd_setImageWithURL(NSURL(string: viewModel.provileViewImage!))
        profileHeaderCellCityLabel.text = viewModel.profileViewCity
        profileHeaderCellAvatarImageView.sd_setImageWithURL(NSURL(string: viewModel.provileViewImage!))
        profileHeaderCellUserNameLabel.text = viewModel.profileViewUserName
        profileHeaderCellInterestLabel.text = viewModel.profileViewUserJob
        profileHeaderCellActionButton.setTitle("Edit Profile", forState: UIControlState.Normal)
        profileHeaderCellFollowersCount.text = viewModel.profileViewFollowsCount
        profileHeaderCellFollowingCount.text = viewModel.profileViewFollowingCount
        profileHeaderCellEventsCount.text = viewModel.profileViewEventsCount
        
        // create effect
        //UIBlurEffect *blur = [UIBlurEffect effectWithStyle:UIBlurEffectStyleLight];
        let blur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let effectView = UIVisualEffectView(effect: blur)
        effectView.frame = self.frame;
        self.profileHeaderCellImageView.addSubview(effectView)
        
        
    }

    @IBAction func editProfileTouchUpInside(sender: AnyObject) {
        
        delegate?.profileHeaderCellEditProfileTouched()
        
    }
    @IBAction func showFollowingButtonTouchUpInside(sender: AnyObject) {
        delegate?.profileHeaderCellShowFollowing()
    }
    @IBAction func showFollowersButtonTouchUpInside(sender: AnyObject) {
        delegate?.profileHeaderCellShowFollowers()
    }
}
