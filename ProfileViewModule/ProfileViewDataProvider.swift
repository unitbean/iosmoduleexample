//
//  ProfileViewDataProvider.swift
//  KickCity
//
//  Created by Nabiullin Anton on 25/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit
protocol ProfileViewDataProviderDelegate:class {
    func profileViewDataProviderDidLoadData()
    func profileViewDataProviderDidFaildWithError(erro:String)
}

class ProfileViewDataProvider: NSObject {
    
    weak var delegate:ProfileViewDataProviderDelegate?
    
    private var userEvents:[FeedItemViewModel] = []
    private var userVisited:[FeedItemViewModel] = []
    private var userInfo:ProfileViewItemViewModel = ProfileViewItemViewModel()
    
    private var sourceType:Int = 0
    
    func setSourceType(type:Int) {
        self.sourceType = type
    }
    
    func getEventsCout() -> Int {
        if self.sourceType == 0 {
            return userEvents.count
        }else{
            return userVisited.count
        }
    }
    
    func getEventsForIndex(index:Int) -> FeedItemViewModel {
        if self.sourceType == 0 {
            return userEvents[index]
        }else{
            return userVisited[index]
        }
    }
    
    private func getUserEvents() {
        KickCityServices.sharedInstance.feedService.requestUserGoingEvents(userInfo.profileViewUserName, offset: userEvents.count) { (results, error) -> Void in
            if error == nil {
                for resultItem in results! {
                    self.userEvents.append(FeedItemViewModel(fromServiceModel: resultItem))
                }
                self.delegate?.profileViewDataProviderDidLoadData()
            }else{
                 self.delegate?.profileViewDataProviderDidFaildWithError(error!)
            }
        }
    }
    
    func loadMoreEvents(userId:String?){
        KickCityServices.sharedInstance.feedService.requestUserGoingEvents(userInfo.profileViewUserName, offset: userEvents.count) { (results, error) -> Void in
            if error == nil {
                for resultItem in results! {
                    self.userEvents.append(FeedItemViewModel(fromServiceModel: resultItem))
                }
                self.delegate?.profileViewDataProviderDidLoadData()
            }else{
                self.delegate?.profileViewDataProviderDidFaildWithError(error!)
            }
        }
    }
    
    func getUserInfoData(userId:String?){
        
        KickCityServices.sharedInstance.profileViewService.getUserInformation(userId) { (results, error) -> Void in
            if error == nil {
                self.userInfo = ProfileViewItemViewModel(fromServiceModel: results!)
               // self.delegate?.profileViewDataProviderDidLoadData()
                self.getUserEvents()
            }else{
                self.delegate?.profileViewDataProviderDidFaildWithError(error!)
            }
        }
        
        
    }
    
    func userInfoLoaded() -> Bool {
        if userInfo.profileViewUserName == nil {
            return false
        }else{
            return true
        }
    }
    
    func getUserInfo() -> ProfileViewItemViewModel {
        return userInfo
    }

}
