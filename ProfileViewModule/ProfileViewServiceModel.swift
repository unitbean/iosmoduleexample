//
//  ProfileViewServiceModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 25/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class ProfileViewServiceModel: NSObject {
    
    var profileViewServiceModelId:String?
    var profileViewServiceModelAvatar:String?
    var profileViewServiceModelFirstName:String?
    var profileViewServiceModelMiddleName:String?
    var profileViewServiceModelLastName:String?
    var profileViewServiceModelEmail:String?
    var profileViewServiceModelStatus:String?
    var profileViewServiceModelJob:String?
    var profileViewServiceModelCity:String?
    var profileViewServiceModelIsAccountPrivate:Bool?
    var profileViewServiceModelSex:String?
   // var profileViewServiceModelSex:String?
    var profileViewServiceModelEventsCount:Int?
    var profileViewServiceModelFollowersCount:Int?
    var profileViewServiceModelIsIamFollowUser:Bool?
    var profileViewServiceModelIsUserFollowMe:Bool?
    var profileViewServiceModelFollowingCount:Int?
    
}
