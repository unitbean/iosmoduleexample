//
//  ProfileViewItemViewModel.swift
//  KickCity
//
//  Created by Nabiullin Anton on 25/02/16.
//  Copyright © 2016 UnitBean. All rights reserved.
//

import UIKit

class ProfileViewItemViewModel {
    
    var provileViewUserId:String?
    
    var provileViewImage:String?
    var profileViewCity:String?
    var profileViewUserName:String?
    var profileViewUserJob:String?
    var profileViewEventsCount:String?
    var profileViewFollowingCount:String?
    var profileViewFollowsCount:String?
    var profileViewFollowsEmail:String?
    
    var profileViewAvatarImageData:NSData?
    
    //var provileViewUserId:String
    
    init(fromServiceModel:ProfileViewServiceModel) {
        self.provileViewImage = KickCityConstants.FILE_URL + fromServiceModel.profileViewServiceModelAvatar!
        self.profileViewCity = fromServiceModel.profileViewServiceModelCity
        self.profileViewUserName = fromServiceModel.profileViewServiceModelFirstName! + " " + fromServiceModel.profileViewServiceModelLastName!
        self.profileViewUserJob = fromServiceModel.profileViewServiceModelJob
        self.profileViewEventsCount = fromServiceModel.profileViewServiceModelEventsCount == 0 ? "0" : String(fromServiceModel.profileViewServiceModelEventsCount!)
        self.profileViewFollowingCount = fromServiceModel.profileViewServiceModelFollowingCount == 0 ? "0" : String(fromServiceModel.profileViewServiceModelFollowingCount!)
        self.profileViewFollowsCount = fromServiceModel.profileViewServiceModelFollowersCount == 0 ? "0" : String(fromServiceModel.profileViewServiceModelFollowersCount!)
        self.profileViewFollowsEmail = fromServiceModel.profileViewServiceModelEmail
        
        self.provileViewUserId = fromServiceModel.profileViewServiceModelId!
    }
    
    init(){
        
    }
    
    

}
